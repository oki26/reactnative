import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, TextInput, Dimensions, TouchableOpacity } from 'react-native';

import bgImage from './images/background.jpg';
import logo from './images/logo.png';
import Icon from 'react-native-vector-icons/Ionicons';

const { width: WIDTH } = Dimensions.get('window');

export default class Example extends Component {
	render() {
		return (
			<ImageBackground source={bgImage} style={styles.backgroundContainer}>
				<View style={styles.logoContainer}>
					<Image source={logo} style={styles.logo} />
					<Text style={styles.logoText}>Selamat Datang</Text>
				</View>

				<View style={styles.inputContainer}>
					{/* <Icon
						name={'ios-person-outLine'}
						size={28}
						color={'rgba(255, 255, 255, 0.7)'}
						style={styles.inputIcon}
					/> */}
					<TextInput
						style={styles.input}
						placeholder={'Username'}
						placeholderTextColor={'rgba(255, 255, 255, 0.4)'}
						underlineColorAndroid="transparent"
					/>
				</View>

				<View style={styles.inputContainer}>
					<TextInput
						style={styles.input}
						placeholder={'Password'}
						secureTextEntry={true}
						placeholderTextColor={'rgba(255, 255, 255, 0.4)'}
						underlineColorAndroid="transparent"
					/>

					{/* <TouchableOpacity style={styles.btnEye} /> */}
				</View>
			</ImageBackground>
		);
	}
}

const styles = StyleSheet.create({
	backgroundContainer: {
		flex: 1,
		width: null,
		height: null,
		alignItems: 'center',
		justifyContent: 'center'
	},
	logoContainer: {
		alignItems: 'center',
		marginBottom: 50
	},
	logo: {
		width: 120,
		height: 120
	},
	logoText: {
		color: 'white',
		fontSize: 20,
		fontWeight: '500',
		marginTop: 10,
		opacity: 0.5
	},
	inputContainer: {
		marginTop: 10
	},
	input: {
		width: WIDTH - 55,
		height: 25,
		borderRadius: 45,
		fontSize: 16,
		paddingLeft: 45,
		backgroundColor: 'rgba(0, 0, 0, 0.35)',
		color: 'rgba(255, 255, 255, 0.7)',
		marginHorizontal: 25
	},
	inputIcon: {
		position: 'absolute',
		top: 10,
		left: 37
	},
	btnEye: {
		position: 'absolute',
		top: 8,
		right: 37
	}
});
